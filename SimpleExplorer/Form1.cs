﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleExplorer
{
    public partial class Form1 : Form
    {
        // Counters for statusBar1 control
        private int nFiles;
        private int nDirectories;

        public Form1()
        {
            InitializeComponent();
            this.nFiles = 0;
            this.nDirectories = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] drives = Environment.GetLogicalDrives();
            dirView.BeginUpdate();

            foreach (string drive in drives)
            {
                TreeNode node = new TreeNode(drive.Remove(2,1));    // remove '/'
                switch (drive)
                {
                    case "A:\\":
                        node.SelectedImageIndex = 0;
                        node.ImageIndex = 0;
                        break;
                    case "C:\\":
                        dirView.SelectedNode = node;    // make dirView_AfterSelect run after this
                        node.SelectedImageIndex = 2;
                        node.ImageIndex = 2;
                        break;
                    case "D:\\":
                        node.SelectedImageIndex = 2;
                        node.ImageIndex = 2;
                        break;
                    case "E:\\":
                        node.SelectedImageIndex = 2;
                        node.ImageIndex = 2;
                        break;
                    case "F:\\":
                        node.SelectedImageIndex = 1;
                        node.ImageIndex = 1;
                        break;
                    default:
                        node.SelectedImageIndex = 3;
                        node.ImageIndex = 3;
                        break;
                }
                dirView.Nodes.Add(node);
            }

            dirView.EndUpdate();
        }

        private void dirView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // Get subdirectories, add to dirView
            addDirectories(e.Node);
            dirView.SelectedNode.Expand();

            // Add list of files to fileView
            addFiles(e.Node.FullPath.ToString());

            // Update statusBar1
            statusBar1.Text = nDirectories.ToString() + " Folder(s), " + nFiles.ToString() + " File(s)";
        }

        private void addDirectories(TreeNode node)
        {
            dirView.BeginUpdate();

            nDirectories = 0;
            try
            {
                DirectoryInfo di;
                if (node.SelectedImageIndex <= 3)
                {
                    // If drive, get directories from drives
                    di = new DirectoryInfo(node.FullPath + "\\");
                }
                else
                {
                    //  Get directories from directories
                    di = new DirectoryInfo(node.FullPath);
                }
                DirectoryInfo[] difs = di.GetDirectories();

                // the directories will get duplicated in treeview if don't do this
                node.Nodes.Clear();

                foreach (DirectoryInfo dif in difs)
                {
                    nDirectories++;
                    TreeNode subNode = new TreeNode(dif.Name);
                    subNode.ImageIndex = 5;
                    subNode.SelectedImageIndex = 6;
                    node.Nodes.Add(subNode);
                }
            }
            catch (Exception exc)
            {
                System.Console.WriteLine(exc.ToString());
                //MessageBox.Show("Error when add directories", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                statusBar1.Text = "Error: Error when add directories.";
            }

            dirView.EndUpdate();
        }

        private void addFiles(string path)
        {
            fileView.BeginUpdate();

            fileView.Items.Clear();
            nFiles = 0;
            try
            {
                DirectoryInfo di = new DirectoryInfo(path + "\\");
                FileInfo[] files = di.GetFiles();
                foreach (FileInfo f in files)
                {
                    nFiles++;
                    ListViewItem item = new ListViewItem(f.Name);
                    item.SubItems.Add(f.LastWriteTime.ToString("dd-MMM-yy hh:mm tt"));
                    item.SubItems.Add(f.Extension.Remove(0,1));
                    long fsize = f.Length / 1024;
                    if (fsize <= 0)
                    {
                        fsize = 1;
                    }
                    item.SubItems.Add((fsize).ToString() + " KB");    //show in KB
                    fileView.Items.Add(item);
                }
            }
            catch (Exception exc)
            {
                System.Console.WriteLine(exc.ToString());
                //MessageBox.Show("Can't view list of files.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                statusBar1.Text = "Error: Can't view list of files.";
            }

            fileView.EndUpdate();
        }

        // open file
        private void fileView_ItemActivate(object sender, EventArgs e)
        {
            try
            {
                string path = dirView.SelectedNode.FullPath;
                string fileName = fileView.FocusedItem.Text;

                Process.Start(path + "\\" + fileName);
            }
            catch (Exception exc)
            {
                System.Console.WriteLine(exc.ToString());
                MessageBox.Show("Can't open file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Simple Explorer by @lvduyanh.", "About");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
